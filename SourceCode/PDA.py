import pandas as pd
import numpy as np
import functools as ft
import operator as op

"Complete the functions(function names, input names and output names are all fixed) and run the python file 'unittestForPandasDataAnalysis.py' to pass the test"
"After passing the test or have any question, pls mail to ningtang@g.ucla.edu"

"This part is about how to use pandas "
"Here are 4 tasks that may help you to have a deeper understanding about pandas and get some basic skilles to use pandas for building Bayesian models"
"Before the specific tasks, I highly recommond you to have a basic knowledge of pandas by going through the website in 10 minutes. link is https://pandas.pydata.org/pandas-docs/stable/10min.html" 

"task 1: get the mean of each comlumn in a dataframe"
"input : a dataframe"
"output : a dataframe with mean value"
"Refence link https://pandas.pydata.org/pandas-docs/stable/10min.html#stats"

def calMeanByPandas(originalDf):
    meanOfOriginalDf = originalDf.mean()
    return meanOfOriginalDf

"task 2.1: create a pandas multiIndex to represent the hypotheses space and initialized the prior probability. Hypotheses space can be is the cartesian product of the variables in your model. (e.g., There are 2 varaibles, height and weight. Height has 3 levels: tall, mid, short; Weight has 3 levels: fat, mid, thin. So there will be 9 hypotheses in the space, 'tall-fat','tall-mid',...Please use pandas multiIndex to do it.)"
"task 2.2: now you have the multiIndex for the hypotheses. Initialize the prior probabilities for them, which should be the equal as the prior is a 'non-informative' uniform distribution. And the sum of prior probabilites shoule be 1"
"input : 1st arguement is a list of lists to represent the different levels of the different variabls, e.g., [[tall, mid, short], [fat, mid, low]]. 2nd argument is a list to represent the variables' names. e.g., ['height', 'weight']"
"output: a dataframe with multiIndex for the hypotheses, and there is a column ['priopP'] whose values are the initialized prior probabilities"
"Refence link https://pandas.pydata.org/pandas-docs/stable/advanced.html"

def createMultiIndexAndInitialUniformPrior(levelsOnDiffVariables, variableNames):
    multiIndex = pd.MultiIndex.from_product(levelsOnDiffVariables, names = variableNames)
    hypothesesNum = len(multiIndex)
    priorP = 1/hypothesesNum
    initialPriorDataFrame = pd.DataFrame([priorP] * hypothesesNum, index = multiIndex, columns = ['priorP'])    
    return initialPriorDataFrame

"task 3: now suppose you have a DataFrame indexed by the multiIndex for the hypotheses. We want to get the values of the hypotheses in the space of the variables we relly cared about, and integerate out the unrelated or latent variables. (i.e., in this task, we want the means of the values in each column, but in the hypotheses space determined by the first variable and last variable.) Use the groupby function in pandas to split the DataFrame to a sub DataFrame which only has the two(1st and 3rd) variables multiindex, and get the means. (i.e., if there are three variables in multiIndex, the output should be the mean of the groupby DataFrame splited by the 1st and 3rd variables) "
"input: a dataframe with multiIndex for the hypotheses and different values in each column" 
"output: a dataframe whose index is a multiIndex only has the first and last variable levels and names, whose values in columns are the means in the hypotheis represented by the index"
"Refence link https://pandas.pydata.org/pandas-docs/stable/advanced.html"

def calGroupbyMeans(multiIndexDf): 
    variableNames = list(multiIndexDf.index.names)
    firstAndLastVariable = [variableNames[0], variableNames[-1]]
    meansOfDataFrameGroupBythefirstAndLastVariables = multiIndexDf.groupby(firstAndLastVariable).mean()
    return meansOfDataFrameGroupBythefirstAndLastVariables

"task 4: now suppose you have a multiIndex for the hypotheses. We want to get the values for every hypothesis from another simple index DataFrame named dataDf. However, as the definition of cartesian product, number of levels for each variable in multiIndex is no longer the same as its original number of levels in dataDf. (i.e. length of two 3-levels variables' cartesian product is 3*3 = 9 --- 'tall, fat', 'tall, mid','tall, thin', 'mid, fat'... So for the first one in the multiIndex, the levels are like 'tall, tall, tall, mid, mid...'). Now we want to get the values for the height in multiIndex from a DataFrame only has three levels index --'tall, mid, short'.)"
"Tips: the height in the multiIndex has 9 levels, not 3 levels. So it is a mapping problem. You can get the labels of the 9 levels, and use the iloc to get the absoulute row value in the 3-level DataFrame. You can name the values column in the original dataDf whatever you like"
"input: 1st argument is a multiIndex. 2nd argument is a one variable simple index Dataframe which has the value for the first varibale of the multiIndex" 
"output: a dataframe whose index levels are the same as the first variable levels in the multiIndex, and whose values in the column are got from the simple one variable index DataFrame named dataDf"

"Refence link https://stackoverflow.com/questions/25405294/how-do-i-extract-the-labels-from-a-multiindex"
"Refence link https://pandas.pydata.org/pandas-docs/stable/indexing.html" "section: selecting data by position"

def testTransDataByMultiIndex(multiIndex, dataDf):
    variableNames = multiIndex.names
    labelsForLevelsInDiffVariables = multiIndex.labels
    firstVariableCorrespondingValueInData = dataDf.iloc[labelsForLevelsInDiffVariables[0]]
    #secondVariableCorrespondingValueInData = dataDf.iloc[labelsForLevelsInDiffVariables[1]]
    #multiIndexDf = pd.DataFrame(list(zip(firstVariableCorrespondingValueInData.values[0], secondVariableCorrespondingValueInData.values[0])), index = multiIndex, columns = [variableNames[0], variableNames[1]])
    return firstVariableCorrespondingValueInData
