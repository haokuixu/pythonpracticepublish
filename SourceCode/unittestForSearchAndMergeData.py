
import unittest
from ddt import ddt, data, unpack
import os
import pandas as pd 
import SearchAndMergeData as targetCode

'''
The name of your script should be "SearchAndMergeData.py"
'''

'''
Task_1 Read filenames. 
There are several subfolders in the Data folder, each includes some data files.
Please write a function to read all the files’ names in any given subfolder.

The name of the function should be readFilenameList. 
The input is the name of the subfolder (eg. “expt1”).
The output is a list with the names of all the files in the given subfolder.

You may need to use the os package of python.
Reference: https://stackoverflow.com/questions/2817264/how-to-get-the-parent-dir-location 
'''
class TestFilenameList(unittest.TestCase):
	def setUp(self):
		self.dirName='expt1'

	def testFilenameListLength(self):
		filenameList=targetCode.readFilenameList(self.dirName)
		filenameListExceptHidden=[i for i in filenameList if not i.startswith('.')]
		self.assertEqual(len(filenameListExceptHidden),20)

	def testFilenameListContent(self):
		filenameList=targetCode.readFilenameList(self.dirName)
		self.assertTrue('sumFile.xlsx' in filenameList)
		self.assertTrue('expt1_humanData.pkl' in filenameList)

	def tearDown(self):
		pass

'''
Task_2 Read absolute path. 
Please write a function to read all the files’ absolute paths in any given subfolder. 

The name of the function should be readAbsPathList. 
The input is the name of the subfolder (eg. “expt1”). 
The output is a list with all the absolute paths of the files in the given subfolder. 

You may need to use the os package of python.
'''
class TestAbsPathList(unittest.TestCase):
	def setUp(self):
		self.dirName='expt1'
		self.parentPath=os.path.abspath(os.path.join(os.getcwd(),os.pardir))

	def testAbsPathListLength(self):
		absPathList=targetCode.readAbsPathList(self.dirName)
		self.assertEqual(len(absPathList),20)

	def testAbsPathListContent(self):
		absPathList=targetCode.readAbsPathList(self.dirName)
		self.assertTrue(self.parentPath+'/Data/'+self.dirName+'/sumFile.xlsx' in absPathList)
		self.assertTrue(self.parentPath+'/Data/'+self.dirName+'/humanSheet.pkl' in absPathList)

	def tearDown(self):
		pass

'''
Task_3 Filter target extension name. 
Please write a function to filter out the filenames with given extension name from the given filename list. 

The name of the function should be filterPathList. 
The inputs are the filename list (or the absolute path list) and a given extension name (eg. “.txt”). 
The output is a list of filenames with the target extension name. 

There are several methods to write the current function. But you may need to use list comprehension method of python.
'''
class TestFilterPathList(unittest.TestCase):
	def setUp(self):
		self.dirName='expt1'
		self.absPathList=targetCode.readAbsPathList(self.dirName)
		self.parentPath=os.path.abspath(os.path.join(os.getcwd(),os.pardir))
		self.targetExtension='.txt'

	def testFilterPathListLength(self):
		filterList=targetCode.filterPathList(self.absPathList, self.targetExtension)
		self.assertEqual(len(filterList),12)

	def testFilterPathListContent(self):
		filterList=targetCode.filterPathList(self.absPathList,self.targetExtension)
		self.assertTrue(self.parentPath+'/Data/'+self.dirName+'/Chasing_-1_xj_Results.txt' in filterList)
		self.assertFalse(self.parentPath+'/Data/'+self.dirName+'/sumFile.xlsx' in filterList)

	def tearDown(self):
		pass

'''
Task_4 Read data. 
Please write a function which could load the data from .txt file into a dataframe. 

The name of the function should be readDataFrameList. 
The input is the result from the above step, which is a list of filenames with the extension name “.txt”. 
The output is a list of dataframes. Each dataframe contains the data from the corresponding .txt file. 

You may need to use the pandas package of python.
Reference: https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html 
'''
class TestDataFrameList(unittest.TestCase):
	def setUp(self):
		self.dirName='expt1'
		self.absPathList=targetCode.readAbsPathList(self.dirName)
		self.targetExtension='.txt'
		self.filterList=targetCode.filterPathList(self.absPathList,self.targetExtension)

	def testDataFrameListLength(self):
		dataFrameList=targetCode.readDataFrameList(self.filterList)
		self.assertEqual(len(dataFrameList),12)

	def testDataFrameListContent(self):
		dataFrameList=targetCode.readDataFrameList(self.filterList)
		self.assertEqual(len(dataFrameList[0].index),224)
		self.assertEqual(dataFrameList[0].index[37],37)
		self.assertEqual(len(dataFrameList[0].columns),11)
		self.assertTrue('chasing_subtlety' in dataFrameList[0].columns)
		self.assertTrue('selected_sheep' in dataFrameList[0].columns)

	def tearDown(self):
		pass

'''
Task_5 Merge data. 
Please write a function which could merge a list of dataframes as one single large dataframe. 

The name of the function should be mergeDataFrame. 
The input is a list of dataframes with same structure. 
The output is a single dataframe that contains all the data in the input dataframes. 

You may need to use the pandas package of python.
Reference: https://pandas.pydata.org/pandas-docs/stable/generated/pandas.concat.html 
'''
class TestMergeDataFrame(unittest.TestCase):
	def setUp(self):
		self.dirName='expt1'
		self.absPathList=targetCode.readAbsPathList(self.dirName)
		self.targetExtension='.txt'
		self.filterList=targetCode.filterPathList(self.absPathList,self.targetExtension)
		self.dataFrameList=targetCode.readDataFrameList(self.filterList)

	def testMergeDataFrame(self):
		resultDataFrame=targetCode.mergeDataFrame(self.dataFrameList)
		self.assertEqual(len(resultDataFrame.index),2688)
		self.assertEqual(resultDataFrame.index[-1],223)
		self.assertEqual(len(resultDataFrame.columns),11)
		self.assertTrue('chasing_subtlety' in resultDataFrame.columns)
		self.assertTrue('selected_sheep' in resultDataFrame.columns)
		self.assertAlmostEqual(resultDataFrame.values.mean(),61.68645156926407)
		self.assertEqual(resultDataFrame.values.sum(),1823945.0)

	def tearDown(self):
		pass

if __name__=="__main__":
	readFilenameListTest=unittest.TestLoader().loadTestsFromTestCase(TestFilenameList)
	unittest.TextTestRunner(verbosity=2).run(readFilenameListTest)
	readAbsPathListTest=unittest.TestLoader().loadTestsFromTestCase(TestAbsPathList)
	unittest.TextTestRunner(verbosity=2).run(readAbsPathListTest)
	filterPathListTest=unittest.TestLoader().loadTestsFromTestCase(TestFilterPathList)
	unittest.TextTestRunner(verbosity=2).run(filterPathListTest)
	dataFrameListTest=unittest.TestLoader().loadTestsFromTestCase(TestDataFrameList)
	unittest.TextTestRunner(verbosity=2).run(dataFrameListTest)
	mergeDataFrameTest=unittest.TestLoader().loadTestsFromTestCase(TestMergeDataFrame)
	unittest.TextTestRunner(verbosity=2).run(mergeDataFrameTest)
