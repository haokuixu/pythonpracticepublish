
import unittest
from ddt import ddt, data, unpack
import os
import numpy as np
import pandas as pd 
import itertools as it
import DiscreteBayesian as targetcode

'''
Task Description:
We are now dealing with a simple bayesian problem.
There are three box, each contains two kinds of balls: red and blue.
The prior of each box are given as dictionary: {'box1':0.2,'box2':0.3,'box3':0.5}
The likelihood of generating ball from each box are given as dictionary of dictionary: {'box1':{'red':0.5,'blue':0.5},'box2':{'red':0.8,'blue':0.2},'box3':{'red':0.75,'blue':0.25}}

The task is amied at building a function to update the posterior from observed data (eg. ['red','red','blue'])
Pleas use DataFrame

The name of your script should be "DiscreteBayesian.py". You may need to build the functions below:
Function initialPriorHypothesis with input the prior dictionary and output the prior dataframe, 
index name is 'boxIdentity', columns name is 'p'
Function initialLikelihoodHypothesis with input the likelihood dictionary and output the likelihood dataframe, 
index name is 'boxIdentity' and 'color', columns name is 'likelihood'

Function calPrior compute the prior of given box. The input should be the box name (eg. 'box1') and the prior dataframe, 
the output is the p value
Function calLikelihood compute the likelihood of given color from given box. The input should be the box name (eg. 'box1') the color (eg. 'red'), 
the output is the p value
Class UpdatePDataFrame initialize with prior dictionary and likelihood dictionary,
and call with input the observed data list (eg. ['red','red','blue'])
the output is a new dataframe with same structure as prior dataframe but the value is updated

By the way, I personaly encourage you to write another function updatePosteriorDataFrame using recursion
the input will be prior dictionary, likelihood dictionary and the observed data list (eg. ['red','red','blue'])
the output will be a new dataframe with same structure as prior dataframe but the value is updated
The most important of such a function is recursion, you should not use any for loop even for a series of data list
There are not a test for such a function here. Pleas communicate with me if you are interested in it:
<haokuixu.psy@gmail.com>  
'''

class TestDiscreteBayesian(unittest.TestCase):
	def setUp(self):
		self.priorDict={'box1':0.2,'box2':0.3,'box3':0.5}
		self.likelihoodDict={'box1':{'red':0.5,'blue':0.5},'box2':{'red':0.8,'blue':0.2},'box3':{'red':0.75,'blue':0.25}}
		self.xList=['box1','box2','box3']
		self.yList=['red','blue']
		hypothesisIndex=pd.MultiIndex.from_product([self.priorDict.keys()],names=['boxIdentity'])
		# self.priorDataFrame=pd.DataFrame(list(self.priorDict.values()),index=hypothesisIndex,columns=['p'])
		self.priorDataFrame=targetcode.initialPriorHypothesis(self.priorDict)
		likelihoodIndex=pd.MultiIndex.from_product([self.xList,self.yList],names=['boxIdentity','color'])
		generatePair=list(it.product(self.xList,self.yList))
		# self.likelihoodDataFrame=pd.DataFrame([self.likelihoodDict[i[0]][i[1]] for i in generatePair],index=likelihoodIndex,columns=['likelihood'])
		self.likelihoodDataFrame=targetcode.initialLikelihoodHypothesis(self.likelihoodDict)
		self.calPosterior=targetcode.UpdatePosteriorDataFrame(self.priorDict, self.likelihoodDict)

	def testPrior(self):
		priorList=[targetcode.calPrior(i,self.priorDataFrame) for i in self.xList]
		self.assertEqual(priorList,list(self.priorDict.values()))
		self.assertEqual(np.sum(priorList),1)

	def testLikelihood(self):
		generatePair=list(it.product(self.xList,self.yList))
		likelihoodList=[targetcode.calLikelihood(i[0],i[1],self.likelihoodDataFrame) for i in generatePair]
		likelihoodGroundTruth=[self.likelihoodDict[i[0]][i[1]] for i in generatePair]
		self.assertEqual(likelihoodList,likelihoodGroundTruth)

	def testPosterior(self):
		self.assertEqual(self.calPosterior(['red']).loc['box1']['p'],0.1/0.715)
		self.assertAlmostEqual(self.calPosterior(['blue']).loc['box3']['p'],0.125/0.285)

	def tearDown(self):
		pass

if __name__=="__main__":
	discreteBayesianTest=unittest.TestLoader().loadTestsFromTestCase(TestDiscreteBayesian)
	unittest.TextTestRunner(verbosity=2).run(discreteBayesianTest)

		

