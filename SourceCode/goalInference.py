import pandas as pd
import numpy as np
import functools as ft
import operator as op
import math 

"Complete the functions(function names, input names and output names are all fixed) and run the python file 'unittestForPandasDataAnalysis.py' to pass the test"
"You can create any sub functions and use in the mainfunction 'goalInfernce', but please do not change the mainfunction name and its arguments names" 
"After passing the test or have any question, pls mail to ningtang@g.ucla.edu"

"This part is about how to infer an agent's goal after its action is observed "
"The basic idea for the inference is from the rationality princeple for human behaviour. When people have a goal(a disired target), they will always approach to it with the least cost."

"task 1 description: suppose there are three resturants located on the [8,2], [2,8], [8,8] positions in the map. now you have an agent's trajectory of moving from the startPoint [2,2]. (e.g., [2,2],[2,3],[3,3],[3,4]). Please return the sequence posterior probabilities of the desired resturants(goals) after each step the agent takes"

"tip 1.1: inference process is based on the generative model. For the generative model, the initial prior probabilities for the resturants are euqal, (i.e., P(goal), goal can be the 1st, 2nd or 3rd resturant) which is [1/3, 1/3, 1/3]. The likelihood distribution for the actions condition on agent's goal, can be different.(i.e., P(action/goal)). The likelihood princeple is the rationality of the huamn behaviour--when we want something, we will take a action to appoch it in a large probabilty, here we set it to 0.9. That means the actions which can make the agent closer to the goal resturant whill share the probality of 0.9, other actions that make it farer to the goal resturant shaore the probability of 0.1. For example, if the agent goal is 3rd resturant, based on the current position [2,2], the probabilities for the actions [1,0],[0,1],[-1, 0],[0, -1] are [0.45, 0.45, 0.05, 0.05]. That is because the actions up[0,1] and right[0,1] both take the agent nearer to the resturant[8, 8] as the current position is [2,2]. Now you have the prior and likelihood. Please calculate the posteriorp(goal/action), based on the basic bayesian rule: p(goal/action) = p(action/goal) * p(goal) / p(action)."

"tip 1.2: the observed action can be got from trajectory. The action type (nearer or farer) can be got based on the current agent position and resturant position"

"tip 1.3: after every action observed, we can get a posterior of goals for this action. It is an array [x, y, z], in which x,y,z is the probability for 1st, 2nd and 3rd resturant. The posterior of the goals condition on the action for the current frame is actually the prior of the goals for the next inference. (i.e. in goal inference, p(goal/action) current = p(goal) next). So now you have a trajectory for three positions, which can provide the information of three actions. Based on the prior and likelihood for each frame, we can get the sequence posterior for the three actions trajectory. (e.g. [[x1,y1,z1], [x2,y2,z2],[x3,y3,z3]], x,y,z is the resturant identity, 1,2,3 is the action identity, which means after 1st, 2nd and 3rd actions.)"

"tip 1.4: use numpy array and python list to finish the task, personal defined sub-function is welcomed to use in the main-function"

"input : positions of the resturants, a list(or array) of list; trajectory from the startpoint, a list(or array) of list, i.e. if the agent have 3 actions, then the trajectory list will have 4 elements, including the start position, position after 1st action, position after 2nd action, position after 3rd action;" 
"other non-input arguments that related to the tasks: actions space, a list(array) of list, fixed, [[1,0],[0,1],[-1,0],[0,-1]]; initial prior, will not be given, can be created in the function or used as a fixed argument, it should be equal, and sum is 1"
"output : a sequence of posterior distributions for each action, a list of list, or numpy array"

def inferGoalGivenTrajectory(goalPositions, trajectory):
    return goalInferencePosteriors

"input data example"
goalPositions = np.array([[8,2],[2,8],[8,8]])
trajectory = np.array([[2,2],[2,3],[3,3],[3,4]])

