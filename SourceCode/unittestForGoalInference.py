import unittest
from ddt import ddt, data, unpack
import pandas as pd
import numpy as np
import functools as ft
import operator as op
import goalInference as targetCode

"Complete the functions in the goalInference.py file(function names, input names and output names are all fixed) and then run the file 'unittestForPandasDataAnalysis.py' to pass the test"
"You can create any sub functions and use in the mainfunction 'goalInfernce', but please do not change the mainfunction name and its arguments names" 
"After passing the test or have any question, pls mail to ningtang@g.ucla.edu"


@ddt
class TestGoalInference(unittest.TestCase):
    def setUp(self):
        pass
    
    @data((np.array([[2, 8], [8, 2], [8, 8]]), np.array([[2, 2], [2, 3], [3, 3], [3, 4]]), np.array([[0.6505, 0.0241, 0.3253], [0.1212, 0.0606, 0.8182], [0.1281, 0.0071, 0.8648]]))) 
    @unpack 
    def testInferGoalGivenTrajactory(self, goalPositions, trajectory, groundTruth):
        output = targetCode.inferGoalGivenTrajectory(goalPositions, trajectory)
        self.assertTrue(np.allclose(output, groundTruth, atol = 1e-3))

    def tearDown(self):
        pass

if __name__ == '__main__':
    goalInferenceSuit = unittest.TestLoader().loadTestsFromTestCase(TestGoalInference)
    unittest.TextTestRunner(verbosity = 2).run(goalInferenceSuit) 
