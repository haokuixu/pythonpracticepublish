import unittest
from ddt import ddt, data, unpack
import pandas as pd
import numpy as np
import functools as ft
import operator as op
import analysisDataByPandas as targetCode
import PDA

"Complete the functions in the 'analysisDataByPandas.py' (function names, input names and output names are all fixed) and run the python file 'unittestForPandasDataAnalysis.py' to pass the test"
"After passing the test or have any question, pls mail to ningtang@g.ucla.edu"

@ddt
class TestAnalysisDataByPandas(unittest.TestCase):
    def setUp(self):
        self.dataDf = pd.DataFrame(list(zip(np.random.normal(100, 100, 100))), columns = ['c'])
        pass

    @data((pd.DataFrame(list(zip(np.random.normal(100, 1000, 100), np.random.uniform(-100, 200, 100))), columns = ['c','d'])), (pd.DataFrame(list(zip(np.random.normal(100, 1000, 100), np.random.uniform(-500, 200, 100))), columns = ['c','d']))) 

    def testCalMeanByPandas(self, originalDf):
        output = targetCode.calMeanByPandas(originalDf)
        truth = PDA.calMeanByPandas(originalDf)
        self.assertTrue(np.allclose(output, truth))
    
    @data(([range(4), range(6), range(5, 20)], ['x','y','z']))

    @unpack
    def testCreateMultiIndexAndInitialUniformPrior(self, levelsOnDiffVariables, variableNames):
        output =  targetCode.createMultiIndexAndInitialUniformPrior(levelsOnDiffVariables, variableNames)
        truth =  PDA.createMultiIndexAndInitialUniformPrior(levelsOnDiffVariables, variableNames)
        self.assertTrue(type(output.index) == type(truth.index))
        self.assertTrue(np.all(output.index == truth.index))
        self.assertTrue(np.all(output.values == truth.values))

    @data((pd.DataFrame(list(zip(np.random.normal(100, 100, 200), np.random.normal(-100, 1000, 200))), index = pd.MultiIndex.from_product([range(4), range(5), range(10)], names = ['x', 'y', 'z']), columns= ['a', 'b'])))
    
    def testCalGroupbyMeans(self, multiIndexDf):
        output = targetCode.calGroupbyMeans(multiIndexDf)
        truth = PDA.calGroupbyMeans(multiIndexDf)
        self.assertTrue(np.all(output.values == truth.values))

    @data((pd.MultiIndex.from_product([range(4), range(5), range(10)], names = ['x', 'y', 'z']))) 
    def testTransDataByMultiIndex(self, multiIndex):
        output = targetCode.testTransDataByMultiIndex(multiIndex, self.dataDf)
        truth = PDA.testTransDataByMultiIndex(multiIndex, self.dataDf)
        self.assertTrue(np.allclose(output, truth))

    def tearDown(self):
        pass

if __name__ == '__main__':
    pandasDataAnalysisSuit = unittest.TestLoader().loadTestsFromTestCase(TestAnalysisDataByPandas)
    unittest.TextTestRunner(verbosity = 2).run(pandasDataAnalysisSuit) 
